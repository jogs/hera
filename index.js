#!/usr/bin/env node --harmony

console.log('Welcome to HERA\n');
var fs = require('fs');
var ProgressBar = require('progress');
var chalk = require('chalk');
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');

program
 .arguments('<file>')
 .option('-u, --username <username>', 'The user to authenticate as')
 .option('-p, --password <password>', 'The user\'s password')
 .action(function(file) {
   console.log('user: %s pass: %s file: %s',
       program.username, program.password, file);
 })
 .parse(process.argv);